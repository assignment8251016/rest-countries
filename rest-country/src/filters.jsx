import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons'
export default function Filters({setSearchText, setSelection}) {
    return (
        <div className="filters">
            <div><FontAwesomeIcon icon={faMagnifyingGlass} />
                <input type="text" placeholder="Search for a country..." 
                onChange={(event)=>{
                        setSearchText(event.target.value)
                    }
                }/>
            </div>

            <select onChange={(event)=>setSelection(event.target.value)}>
                <option value="" disabled selected hidden>filter by region</option>
                <option value="Africa" >africa</option>
                <option value="Americas">america</option>
                <option value="Asia">asia</option>
                <option value="Europe">europe</option>
                <option value="Oceania">oceania</option>
            </select>
        </div>
    )
}