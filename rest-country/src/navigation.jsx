import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMoon } from '@fortawesome/free-solid-svg-icons'
export default function Navigation(){
    return (
        <nav className="nav">
            <a id="head-line" href="">where in the world?</a> 
            <a id='dark-mode'>
                <FontAwesomeIcon icon={faMoon} /> dark mode
            </a>
        </nav>
    );
}