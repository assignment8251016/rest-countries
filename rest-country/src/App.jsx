import React from 'react'
import ReactDOM from 'react-dom/client'
import { useEffect, useState } from 'react'
import Home from './Home.jsx'
import './App.css'
import axios from 'axios';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Country from './Country.jsx'
import Navigation from './navigation'

async function getcountryData(setTo, setLoading){
  let countryData=null;
  countryData = await axios.get("https://restcountries.com/v3.1/all").then(res=>{
    setLoading(false);
   return res.data});
  setTo(countryData);
}

export default function App(){
  const [countryData, setcountryData] = useState(undefined);
  const [searchText, setSearchText] = useState("");
  const [selection, setSelection] = useState("");
  const [id, setId] = useState(undefined);
  const [isLoading, setLoading] = useState("true");


  useEffect(()=>{
      getcountryData(setcountryData, setLoading);
  }, [searchText, selection])


  let filteredCountries = [];
  if(countryData!==undefined){
    filteredCountries = countryData.filter(
      (country) =>{
        console.log(country.region,  selection);
        return country.name.common.toLowerCase().startsWith(searchText.toLowerCase()) &&
        (selection === "" || country.region === selection)
      }
    )
  }
   
  return (
    <Router>
      <Navigation/>
       <Routes>
        <Route path="/" element={<Home countryData={filteredCountries} setSearchText={setSearchText} setSelection={setSelection} setId={setId} isLoading={isLoading}/>} />
        <Route path={`/country/:id`} element={<Country id={id} setSearchText={setSearchText} setSelection={setSelection}/>} />
      </Routes>
    </Router>
  );
}



