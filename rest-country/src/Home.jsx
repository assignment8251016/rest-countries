import Filters from './filters'
import Card from './card';

function Home({ setSearchText, setSelection, countryData, setId, isLoading}) {

  return (
    <>
      <Filters setSearchText={setSearchText} setSelection={setSelection} />
      <div className='card-container'>
       
        { isLoading? (<h1 className='not-found'>Loading...</h1>):(!(countryData===undefined)) && countryData.length > 0 ? (countryData.map((data, index) => {
          return <Card key={index} data={data} setId={setId} />
        })) : (<h1 className='not-found'>No Data Found!</h1>)}
      </div>
    </>
  )
}

export default Home
