import { faArrowLeftLong } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import axios from "axios";
import { useEffect, useState } from "react";
import { Link, Route, Router, useParams } from 'react-router-dom';
import Home from './Home';

export default function Country({setSearchText, setSelection}){
    const {id} = useParams();
    const [country, setCountry] = useState(undefined);


    console.log("id:",id);
    useEffect(()=>{
        axios.get(`https://restcountries.com/v3.1/alpha/${id}`).then(response => setCountry(response.data[0]))
    },[id]);
    console.log(country)

    return (
        <> {
            (!(country===undefined))?
        <div className="country">
            <Link to={`/`} >
            <button onClick={()=>{
                setSearchText("");
                setSelection("");
                <Route path={`/`} element={<Home/>} />
            }}> <FontAwesomeIcon icon={faArrowLeftLong} /> back</button></Link>
            <div key={country.cca3} className="container">
                <img src={country.flags.svg} alt="" />
                <div className="information">   
                    <h2>{country.name.common}</h2>
                    <div className="sub-info">
                        <div className="name">
                            <p>native name:</p>
                            <p>{country.name.common}</p>
                        </div>
                        <div className="population">
                            <p>population:</p>
                            <p>{country.population}</p>
                        </div>
                        <div className="region">
                            <p>region:</p>
                            <p>{country.region}</p>
                        </div>
                        <div className="sub-region">
                            <p>sub region:</p>
                            <p>{country.subregion}</p>
                        </div>
                        <div className="capital">
                            <p>capital:</p>
                            <p>{country.capital}</p>
                        </div>
                        <div className="top-level-domian">
                            <p>top level domain</p>
                            {/* {country.tld.map((t)=><p key={t}>{t}</p>)} */}
                        </div>
                        <div className="currencies">
                            <p>currencies:</p>
                            <p>{country.currencies &&  Object.entries(country.currencies).length>0 && Object.entries(country.currencies)[0][1].name}</p>
                        </div>
                        <div className="language">
                            <p>language:</p>
                            <p>{(country.languages) && Object.entries(country.languages)[0][1]}</p>
                        </div>
                    </div>
                    <div className="border-contries">
                        <p >border countries</p>
                        <div className='borders'>
                            {country.borders && country.borders.map(t=>
                            <Link to={`../country/${t}`} key={t} >
                                <button onClick={()=>{
                                // setAid(t);
                                <Route path={`/country/:id`} element={<Country />} />
                                }}>{t}</button>
                            </Link>)}
                        </div>
                    </div>
                </div>
            </div>
        </div>
            :<div className='loading'>
                <h1>Loading</h1>
            </div>

        
        }
        </>
    )
}