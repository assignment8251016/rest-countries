import { Link } from "react-router-dom";
export default function Card({data, setId}){

    return (
        <Link to={`/country/${data.cca3}`} className="card" onClick={()=>{
            setId(data.cca3);
        }}>
            <img src={data.flags.png}/>
            <div className="content">
                <p className="country">{data.name.common}</p>
                <div className="country-info">
                    <div>
                        <p className="population">population</p>
                        <p className="value">{data.population}</p>
                    </div>
                    <div>
                        <p className="region">region</p>
                        <p className="value">{data.region}</p>
                    </div>
                    <div>
                        <p className="capital">capital</p>
                        <p className="value">{data.capital}</p>
                    </div>
                </div>
            </div>
        </Link>
    )
}